import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import org.kde.plasma.components 3.0 as PlasmaComponents3
import QtQuick.Extras 1.4

RowLayout {
     anchors.fill:parent
    Layout.maximumHeight: backbutton.width
    Rectangle {
    id: status
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.minimumHeight: 24 
    Layout.minimumWidth: 24
    color: "transparent"

    StatusIndicator {
        anchors.centerIn: parent
        Layout.margins: 8
        Layout.fillWidth: true
        Layout.fillHeight: true
        width: 24
        color: "green"
    }
}
    PlasmaComponents3.ToolButton {
                    id: backbutton
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: width 
                    Layout.minimumWidth: 34
                    icon.name: "media-seek-backward"
                    onClicked: {
                    
                    }
                }
    PlasmaComponents3.ToolButton {
                    id: stopbutton
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: width 
                    Layout.minimumWidth: 34
                    icon.name: "media-playback-stop"
                    onClicked: {
                    ;
                    }
                }
    PlasmaComponents3.ToolButton {
                    id: playbutton
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: width 
                    Layout.minimumWidth: 34
                    checkable: true
                    icon.name: LayoutMirroring.enabled , checked ? "media-playback-pause" : "media-playback-start"
                    onToggled: {
                    
                    }
                }
    PlasmaComponents3.ToolButton {
                    id: forwardbutton
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: width 
                    Layout.minimumWidth: 34
                    icon.name: "media-seek-forward"
                    onClicked: {
                    
                    }
                }
}
 
