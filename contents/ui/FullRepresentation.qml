import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import org.kde.plasma.components 3.0 as PlasmaComponents3
import QtQuick.Extras 1.4

RowLayout { 
    Rectangle {
    id: status
    width: 64
    height: 64
    color: "transparent"

    StatusIndicator {
        anchors.centerIn: parent
        color: "green"
        active: true
    }
}
  
    PlasmaComponents3.ToolButton {
                    id: backbutton
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: width 
                    Layout.minimumWidth: 64
                    anchors.fill:parent.left
                    icon.name: "media-seek-backward"
                    onClicked: {
                    }
                }
    PlasmaComponents3.ToolButton {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: width 
                    Layout.minimumWidth: 64
                    anchors.fill:parent.left
                    icon.name: "media-playback-stop"
                    onClicked: {
                    
                    }
                }
    PlasmaComponents3.ToolButton {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: width 
                    Layout.minimumWidth: 64
                    checkable: true
                    icon.name: LayoutMirroring.enabled , checked ? "media-playback-pause" : "media-playback-start"
                    onToggled: {
                   
                    }
                }
    PlasmaComponents3.ToolButton {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: width 
                    Layout.minimumWidth: 64
                    icon.name: "media-seek-forward"
                    onClicked: {
                    
                    }
                }

                PlasmaComponents3.Label {
            Layout.alignment: Qt.AlignCenter
            text: plasmoid.nativeInterface.nativeText
        }
}


